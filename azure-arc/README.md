azure-arc
=========

This role will install Azure-Arc in Linux and Windows machine as well.
When the playbook runs it gathers facts about the machine and determine the ansible_distribution and runs role according to the ansible_distribution.

Steps done in Installing Azure-Arc
1. Check if earlier version of azcmagent is installed and remove it to install the latest version of Arc-Agent
2. Disconnect from Resource Group if it has already been connected by "azcmagent connect" command
3. Download the azcmagent installation script
4. Run the script to install azcmagent package
5. Use the azcmagent connect command to connect the arc-agent with the RG

Requirements
------------

This role only supports installation on:
- ansible_distribution: Microsoft Windows Server 2012 and higher version
- ansible_distribution: RedHat 7(X64) and higher version

Role Variables
--------------

The following variable must be set when using this role.
These variable are the variables used by the "azcmagent connect" command to connect the host with the Azure Resource Group.
We can edit these variables according to the needs. These variables are stored in defaults/main.yml

    azure_arc_agent_resourceGroup=                    <CHANGE-ME>
    azure_arc_agent_tenantId=                         <CHANGE-ME>
    azure_arc_agent_location=                        <CHANGE-ME>
    azure_arc_agent_subscription=                     <CHANGE-ME>
    azure_arc_agent_tags=                             <CHANGE-ME>
    azure_arc_agent_service_principal_id=             <CHANGE-ME>
    azure_arc_agent_service_principal__secret=        <CHANGE-ME>

~~ Notes on "azure_arc_agent_location"
The Location value currently provided by Azure-Arc agent are:
 - EastUS
 - WestUS
 - WestEurope
 - SoutheastAsia


Example Playbook
---------------

    - hosts: servers
      gather_facts: yes
      roles:
         - role: azure-arc

License
-------

BSD

